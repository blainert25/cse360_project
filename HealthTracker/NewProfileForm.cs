﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthTracker
{
    public partial class NewProfileForm : Form
    {
        public NewProfileForm()
        {
            InitializeComponent();
        }

        private void submitButton_Click(object sender, EventArgs e)
        {
            String username, password, confirmPassword;
            bool exists;
            int height;
            username = usernameTextBox.Text ?? "";
            password = passwordTextBox.Text ?? "";
            confirmPassword = confirmPasswordTextBox.Text ?? "";

            if (username == "")
            {
                MessageBox.Show("You must fill in username.");
                return;
            }
            try 
            {
                height = Convert.ToInt32(heightTextBox.Text);
            }
            catch (FormatException exc)
            {
                MessageBox.Show("Your height could not be read. Please enter in your height in inches (eg: 58)");
                return;
            }
            if (password != confirmPassword)
            {
                MessageBox.Show("The passwords do not match. Please re-enter your password.");
                passwordTextBox.Text = "";
                confirmPasswordTextBox.Text = "";
            }

            using (var dbctx = new PHTDB())
            {
                var userList = dbctx.users.SqlQuery("SELECT * FROM dbo.users WHERE username = '" + username + "'").ToList();
                if (userList.Count == 0) exists = false;
                else exists = true;
            }
            if (exists)
            {
                var result = MessageBox.Show("This profile already exists. Would you like to log in now?", "Profile Already Exists", MessageBoxButtons.YesNo);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    LoginForm.reusername = username;
                    this.Close();
                }
            }
            else
            {
                User temp = new User() { username = username, password = password, height = height, autologin = false, alias = Environment.UserName };
                using (var dbctx = new PHTDB())
                {
                    dbctx.users.Add(temp);
                    dbctx.SaveChanges();
                }
                MessageBox.Show("User Profile Created");
                LoginForm.reusername = username;
                this.Close();
            }

        }//end of submitButton_Click

        private void NewProfileForm_Load(object sender, EventArgs e)
        {
            
        }


    }
}

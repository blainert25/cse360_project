﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HealthTracker
{
    class HealthInfoController
    {
        public HealthInfoController() { }

        public float calculateBMI()
        {
            float bmi = 1;
            int height;
            float weight;
            if (MainForm.user != null)
            {
                height = MainForm.user.height;
            }
            else
            {
                //TODO: Need to fix this with actual logic using PHTDB and getting height from user in database. 
                height = 0;
            }
            //Here we'll fetch the users 
            // do stuff
            using (var db = new PHTDB())
            {
                var wList = (from x in db.data
                        where x.username.Equals(MainForm.username)
                        select new {x.datetime,x.weight}).ToList();
                wList.OrderBy(x => x.datetime);
                weight = wList.First().weight;
            }
            bmi = (weight /(height * height))*703;
            return bmi;
        }

    }//end of HealthInfoController class
}

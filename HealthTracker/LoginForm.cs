﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthTracker
{
    public partial class LoginForm : Form
    {
        private int attempts = 0;
        public static String reusername { get; set; }

        public LoginForm()
        {
            InitializeComponent();

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            String username = this.usernameTextBox.Text ?? "";
            String password = this.passwordTextBox.Text ?? "";

            if (username == "")
            {
                MessageBox.Show("The username cannot be left empty.");
                return;
            }

            User usr;
            using (var dbctx = new PHTDB())
            {
                var result = dbctx.users.SqlQuery("SELECT * FROM dbo.users WHERE username = '" + username + "'");//.ToList().First();
                if (result.Count() == 0)
                {
                    usr = null;
                }
                else usr = result.ToList().First();
            }

            if ((usr != null) && (usr.password == password))
            {
                Program.username = username;
                Program.authorized = true;
                this.Close();
            }
            else
            {
                attempts++;
                MessageBox.Show("The Username or Password entered is not correct.\nAttempt: " + attempts);
            }

        }

        private void newButton_Click(object sender, EventArgs e)
        {
            new NewProfileForm().ShowDialog(this);

        }

        private void LoginForm_Load(object sender, EventArgs e)
        {
            //Need to remember to check the database for the Automatic login and if so, then skip this and go to MainForm
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HealthTracker
{
    class HealthInfo
    {

        public HealthInfo()
        {

        }

        
        public String username { get; set; }
        [Key]
        public long datetime { get; set; }
        public float cardioHours { get; set; }
        public float strengthTrainingHours { get; set; }
        public float workHours { get; set; }
        public float sleptHours { get; set; }
        public int pulse { get; set; }
        public int bloodPressureSys { get; set; }
        public int bloodPressureDys { get; set; }
        public float weight { get; set; }
        public int calories { get; set; }
        public float bodyMassIndex { get; set; }



    }//end of HealthInfo Class
}

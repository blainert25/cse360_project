﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Entity;



//Just adding the shell file for SQL Connection class 
//Example of my LocalAppData: C:\Users\rzwiefel\AppData\Local
//Hello?
namespace HealthTracker
{
    class PHTDB : DbContext
    {
        public PHTDB() : base("HealthTrackerDatabase")
        {
            Database.SetInitializer<PHTDB>(new DropCreateDatabaseIfModelChanges<PHTDB>());
        }

        public DbSet<User> users { get; set; }
        public DbSet<HealthInfo> data { get; set; }


    }//End of SQLConn class
}//End of HealthTracker Namespace

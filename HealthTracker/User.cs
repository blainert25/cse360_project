﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace HealthTracker
{
    public class User
    {
        public User() { }

        [Key]
        [Required(ErrorMessage = "Username is Required")]
        public String username { get; set; }

        public String password { get; set; }
        public String alias { get; set; }
        public int height { get; set; }
        public bool autologin { get; set; }


    }//end of User class
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HealthTracker
{
    public partial class MainForm : Form
    {
        public static String username;
        public static User user;

        public MainForm(String username)
        {
            InitializeComponent();
            MainForm.username = username;
            using (var db = new PHTDB())
            {
                user = (from x in db.users
                        where x.username.Equals(username)
                        select x).First();
            }
        }


        private void MainForm_Load(object sender, EventArgs e)
        {
            this.welcomeLabel.Text = "Welcome, " + (username ?? "Unkown") + "!";
        }

        private void btnSave_Click_1(object sender, EventArgs e)
        {
            DateTime now = DateTime.Now;
            float sleep=0, work=0, strength=0, cardio=0, weight=0;
            int calories=0, systolicBP=0, diastolicBP=0, pulse=0;
            bool error = false;

            try
            {
                cardio = (float)Convert.ToDouble(this.txtCardio.Text);
                this.txtCardio.BackColor = System.Drawing.Color.White;
                this.errorProvider.SetError(this.txtCardio, String.Empty);
            }
            catch (Exception ex)
            {
                this.txtCardio.BackColor = System.Drawing.Color.LightCoral;
                this.errorProvider.SetError(this.txtCardio, "Please enter a valid Value");
                error = true;
            }

            try
            {
                strength = (float)Convert.ToDouble(this.txtStrength.Text);
                this.txtStrength.BackColor = System.Drawing.Color.White;
                this.errorProvider.SetError(this.txtStrength, String.Empty);
            }
            catch (Exception ex)
            {
                this.txtStrength.BackColor = System.Drawing.Color.LightCoral;
                this.errorProvider.SetError(this.txtStrength, "Please enter a valid value");
                error = true;
            }

            try
            {
                work = (float)Convert.ToDouble(this.txtWork.Text);
                this.txtWork.BackColor = System.Drawing.Color.White;
                this.errorProvider.SetError(this.txtWork, String.Empty);
            }
            catch (Exception ex)
            {
                this.txtWork.BackColor = System.Drawing.Color.LightCoral;
                this.errorProvider.SetError(this.txtWork, "Please enter a valid value");
                error = true;
            }

            try
            {
                sleep = (float)Convert.ToDouble(this.txtSleep.Text);
                this.txtSleep.BackColor = System.Drawing.Color.White;
                this.errorProvider.SetError(this.txtSleep, String.Empty);
            }
            catch (Exception ex)
            {
                this.txtSleep.BackColor = System.Drawing.Color.LightCoral;
                this.errorProvider.SetError(this.txtSleep, "Please enter a valid value");
                error = true;
            }

            try
            {
                pulse = Convert.ToInt32(this.txtPulse.Text);
                this.txtPulse.BackColor = System.Drawing.Color.White;
                this.errorProvider.SetError(this.txtPulse, String.Empty);
            }
            catch (Exception ex)
            {
                this.txtPulse.BackColor = System.Drawing.Color.LightCoral;
                this.errorProvider.SetError(this.txtPulse, "Please enter a valid value");
                error = true;
            }

            try
            {
                systolicBP = Convert.ToInt32(this.txtSystolicBP.Text);
                diastolicBP = Convert.ToInt32(this.txtDiastolicBP.Text);
                this.txtSystolicBP.BackColor = System.Drawing.Color.White;
                this.txtDiastolicBP.BackColor = System.Drawing.Color.White;
                this.errorProvider.SetError(this.txtSystolicBP, String.Empty);
            }
            catch (Exception ex)
            {
                this.txtSystolicBP.BackColor = System.Drawing.Color.LightCoral;
                this.txtDiastolicBP.BackColor = System.Drawing.Color.LightCoral;
                this.errorProvider.SetError(this.txtSystolicBP, "Please enter a valid value");
                error = true;
            }

            try
            {
                weight = (float)Convert.ToDouble(this.txtWeight.Text);
                this.txtWeight.BackColor = System.Drawing.Color.White;
                this.errorProvider.SetError(this.txtWeight, String.Empty);
            }
            catch (Exception ex)
            {
                this.txtWeight.BackColor = System.Drawing.Color.LightCoral;
                this.errorProvider.SetError(this.txtWeight, "Please enter a valid value");
                error = true;
            }

            try
            {
                calories = Convert.ToInt32(this.txtCalories.Text);
                this.txtCalories.BackColor = System.Drawing.Color.White;
                this.errorProvider.SetError(this.txtCalories, String.Empty);
            }
            catch (Exception ex)
            {
                this.txtCalories.BackColor = System.Drawing.Color.LightCoral;
                this.errorProvider.SetError(this.txtCalories, "Please enter a valid value");
                error = true;
            }

            if (error) return;
            else
            {
                HealthInfo hi = new HealthInfo();
                HealthInfoController hiBMI = new HealthInfoController();
                hi.username = username;
                hi.datetime = now.ToFileTimeUtc();
                hi.cardioHours = cardio;
                hi.strengthTrainingHours = strength;
                hi.workHours = work;
                hi.sleptHours = sleep;
                hi.pulse = pulse;
                hi.bloodPressureSys = systolicBP;
                hi.bloodPressureDys = diastolicBP;
                hi.weight = weight;
                hi.calories = calories;
                hi.bodyMassIndex = hiBMI.calculateBMI(); //TODO: still need to actually compute this 

                using (var dbctx = new PHTDB())
                {
                    dbctx.data.Add(hi);
                    dbctx.SaveChanges();
                }
            }

            MessageBox.Show("Data Saved Successfully");
            this.txtCalories.Text = "";
            this.txtCardio.Text = "";
            this.txtDiastolicBP.Text = "";
            this.txtPulse.Text = "";
            this.txtSleep.Text = "";
            this.txtStrength.Text = "";
            this.txtSystolicBP.Text = "";
            this.txtWeight.Text = "";
            this.txtWork.Text = "";


        }//end of save button

        private void txtCardio_TextChanged(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(txtCardio.Text, out x) && (this.txtCardio.Text != ""))
            {
                this.errorProvider.SetError(this.txtCardio, "Input must be a real number");
            }
            else
            {
                this.errorProvider.SetError(this.txtCardio, String.Empty);
            }
        }

        private void txtStrength_TextChanged(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(txtStrength.Text, out x) && (this.txtStrength.Text != ""))
            {
                this.errorProvider.SetError(this.txtStrength, "Input must be a real number");
            }
            else
            {
                this.errorProvider.SetError(this.txtStrength, String.Empty);
            }
        }

        private void txtWork_TextChanged(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(txtWork.Text, out x) && (this.txtWork.Text != ""))
            {
                this.errorProvider.SetError(this.txtWork, "Input must be a real number");
            }
            else
            {
                this.errorProvider.SetError(this.txtWork, String.Empty);
            }
        }

        private void txtSleep_TextChanged(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(txtSleep.Text, out x) && (this.txtSleep.Text != ""))
            {
                this.errorProvider.SetError(this.txtSleep, "Input must be a real number");
            }
            else
            {
                this.errorProvider.SetError(this.txtSleep, String.Empty);
            }
        }

        private void txtPulse_TextChanged(object sender, EventArgs e)
        {
            int x;
            if (!int.TryParse(txtPulse.Text, out x) && (this.txtPulse.Text != ""))
            {
                this.errorProvider.SetError(this.txtPulse, "Input must be an integer");
            }
            else
            {
                this.errorProvider.SetError(this.txtPulse, String.Empty);
            }
        }

        private void txtDiastolicBP_TextChanged(object sender, EventArgs e)
        {
            int x;
            if (!int.TryParse(txtDiastolicBP.Text, out x) && (this.txtDiastolicBP.Text != ""))
            {
                this.errorProvider.SetError(this.txtDiastolicBP, "Input must be an integer");
            }
            else
            {
                this.errorProvider.SetError(this.txtDiastolicBP, String.Empty);
            }
        }

        private void txtSystolicBP_TextChanged(object sender, EventArgs e)
        {
            int x;
            if (!int.TryParse(txtSystolicBP.Text, out x) && (this.txtSystolicBP.Text != ""))
            {
                this.errorProvider.SetError(this.txtSystolicBP, "Input must be an integer");
            }
            else
            {
                this.errorProvider.SetError(this.txtSystolicBP, String.Empty);
            }
        }

        private void txtWeight_TextChanged(object sender, EventArgs e)
        {
            double x;
            if (!double.TryParse(txtWeight.Text, out x) && (this.txtWeight.Text != ""))
            {
                this.errorProvider.SetError(this.txtWeight, "Input must be a real number");
            }
            else
            {
                this.errorProvider.SetError(this.txtWeight, String.Empty);
            }
        }

        private void txtCalories_TextChanged(object sender, EventArgs e)
        {
            int x;
            if (!int.TryParse(txtCalories.Text, out x) && (this.txtCalories.Text != ""))
            {
                this.errorProvider.SetError(this.txtCalories, "Input must be an integer");
            }
            else
            {
                this.errorProvider.SetError(this.txtCalories, String.Empty);
            }
        }

        //Display PHTGraphics form with table and graphs
        private void btnDisplay_Click(object sender, EventArgs e)
        {
            new PHTGraphics().ShowDialog(this);
        }

        private void displayToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnDisplay_Click(sender, e);
        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            btnSave_Click_1(sender, e);
        }

        private void clearDatabaseToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var result = MessageBox.Show("Are you sure you want to erase the entire database? This is irreversable.", "Wipe Database", MessageBoxButtons.YesNo);
            if (result == System.Windows.Forms.DialogResult.Yes)
            {
                var db = new PHTDB();
                db.Database.ExecuteSqlCommand("TRUNCATE TABLE dbo.healthinfoes"); //Deletes entire table
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }


        


    }//end of MainForm Class
}
